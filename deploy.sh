docker build -t ryu9527/multi-client -f ./client/Dockerfile ./client
docker build -t ryu9527/multi-server -f ./server/Dockerfile ./server
docker build -t ryu9527/multi-worker -f ./worker/Dockerfile ./worker
docker push ryu9527/multi-client
docker push ryu9527/multi-server
docker push ryu9527/multi-worker
kubectl apply -f k8s
kubectl rollout restart deployment/server-deployment
kubectl rollout restart deployment/client-deployment
kubectl rollout restart deployment/worker-deployment